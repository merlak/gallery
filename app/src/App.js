import React, { useState, useEffect } from 'react';
import './App.css';
import Axios from 'axios';
import 'bootstrap/dist/css/bootstrap.min.css';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Image from 'react-bootstrap/Image';
import Button from 'react-bootstrap/Button';
import CONFIG from './config';

function App() {
  const [hasError, setErrors] = useState(false);
  const [venues, setVenues] = useState([]);
  const [discountPercentage, setDiscountPercentage] = useState(0);
  const [name, setName] = useState('');
  const [queryString, setQueryString] = useState('');

  function fetchData() {
    Axios.get(CONFIG.API_URI + '/api/venues' + (queryString.length > 0 ? '?' + queryString : ''))
      .then(function(response) {
        // Unset any previous error:
        setErrors(false);
        setVenues(response.data);
      })
      .catch(function(error) {
        setErrors(error);
      });
  }

  useEffect(() => {
    fetchData();
  }, [queryString]);

  const discountPercentageChangeHandler = event => {
    const newValue = event.target.value;
    if (isNaN(newValue) || 0 > newValue || 100 < newValue) {
      // TODO - instead of alerts some inline validation messages could be added as an improvement.
      alert('You can only enter numbers between 0 and 100!');
      return;
    }
    setDiscountPercentage(event.target.value);
  };

  // TODO At the moment only Apply button triggers submit. This could be extended to detectng user
  // pressing enter inside input fields.
  const submitHandler = () => {
    let params = {};
    if (0 < discountPercentage) {
      params['discountPercentage'] = discountPercentage;
    }
    if (0 < name.trim().length) {
      params['name'] = name;
    }
    setQueryString(
      Object.keys(params)
        .map(key => key + '=' + params[key])
        .join('&')
    );
  };

  return (
    <div className="App">
      <Row className="text-center mb-5">
        <Col xs={12}>
          <label className="pr-2" htmlFor="discountPercentage">
            Discount Percentage:
          </label>
          <input
            id="discountPercentage"
            name="discountPercentage"
            type="number"
            onChange={discountPercentageChangeHandler}
            value={discountPercentage}
            style={{ width: '50px' }}
          />
        </Col>
        <Col xs={12}>
          <label className="pr-2" htmlFor="name">
            Venue name:
          </label>
          <input
            id="name"
            name="name"
            type="text"
            onChange={event => setName(event.target.value)}
            value={name}
            style={{ width: '200px' }}
          />
        </Col>
        <Col xs={12}>
          <Button variant="primary" size="lg" onClick={submitHandler}>
            Apply Filters
          </Button>
        </Col>
      </Row>

      {hasError ? (
        <Row>
          <Col>
            {/* We wouldn't really wan't to display this error to the user
              but for demo purpose I'm adding it here. */}
            Error fetching venues: {JSON.stringify(hasError)}
          </Col>
        </Row>
      ) : (
        <Row>
          {venues.length === 0 ? (
            <Col>No results for specified filter!</Col>
          ) : (
            venues.map(venue => {
              // TODO - another imporvement that could be done here is lazy loading of images since the list
              // might be big.
              return (
                <Col key={venue.id} className="col-md-4">
                  <div>
                    <a href={venue.image} target="_blank" rel="noopener noreferrer">
                      <Image thumbnail src={venue.image} alt="Lights" fluid />
                      <div className="caption mb-3">
                        <div>{venue.name}</div>
                        <div>Discount %: {venue.discount_percentage}</div>
                      </div>
                    </a>
                  </div>
                </Col>
              );
            })
          )}
        </Row>
      )}
    </div>
  );
}

export default App;
