<?php

namespace App\Controller;

use Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Encoder\XmlEncoder;

class GalleryApiController
{
    private const JSON_FILE_MISSING_OR_CORRUPT = 'JSON_FILE_MISSING_OR_CORRUPT';

    /**
     * @Route(
     *     "/api/venues{format}",
     *     name="_api_venues",
     *     methods={"GET"},
     *     requirements={"format" = "(.json|.xml)?"}
     * )
     * @param Request $request
     * @param string $format
     * @return Response
     */
    public function venuesAction(Request $request, string $format)
    {
        try {
            $venuesJson = file_get_contents(__DIR__ . '/../../data/input.json');
            $venues = json_decode($venuesJson, true);
            if (!$venues) {
                return new Response(self::JSON_FILE_MISSING_OR_CORRUPT, 412);
            }
        } catch (Exception $e) {
            error_log($e->getMessage());

            return new Response(self::JSON_FILE_MISSING_OR_CORRUPT, 412);
        }

        // FILTERING
        $discountPercentageFilter = $request->query->get('discountPercentage', null);
        $nameFilter = $request->query->get('name', null);
        // If no filters are set return results.
        if (null === $discountPercentageFilter && null === $nameFilter) {
            return $this->getFormattedResponse($venues, $format);
        }

        // Otherwise apply filters.
        $venues = array_values(array_filter($venues, function ($venue) use ($discountPercentageFilter, $nameFilter) {
            return (null === $discountPercentageFilter || $venue['discount_percentage'] >= $discountPercentageFilter)
                && (null === $nameFilter || false!== mb_stripos($venue['name'], $nameFilter));
        }));

        return $this->getFormattedResponse($venues, $format);
    }

    /**
     * Helper function to get the response in desired format.
     * @param $responseData
     * @param $format
     * @return JsonResponse|Response
     */
    private function getFormattedResponse($responseData, $format)
    {
        if ('.xml' === $format) {
            $encoder = new XmlEncoder();
            $response = new Response();
            $response->headers->set("Content-type", "text/xml");
            $response->setContent($encoder->encode($responseData, 'xml'));

            return $response;
        }

        return new JsonResponse($responseData, 200);
    }
}
