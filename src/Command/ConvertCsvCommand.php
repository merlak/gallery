<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Serializer\Encoder\CsvEncoder;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class ConvertCsvCommand extends Command
{
    protected static $defaultName = 'app:convert-csv';

    protected function configure()
    {
        $this->setDescription('csv parsing command');
    }

    protected function execute(InputInterface $input, OutputInterface $output): void
    {
        $io = new SymfonyStyle($input, $output);
        $encoders = [new CsvEncoder(), new XmlEncoder(), new JsonEncoder()];
        $serializer = new Serializer([new ObjectNormalizer()], $encoders);

        $dataDirectoryPath = __DIR__.'/../../data/';
        $csvData = $serializer->decode(file_get_contents($dataDirectoryPath.'input.csv'), 'csv');
        $jsonData = $serializer->serialize($csvData, 'json');
        $xmlData = $serializer->serialize($csvData, 'xml');

        file_put_contents($dataDirectoryPath.'input.json', $jsonData);
        file_put_contents($dataDirectoryPath.'input.xml', $xmlData);

        $io->success('Conversion finished!');
    }
}
